package com.demonpilot.medicalconferences.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.demonpilot.medicalconferences.Entities.Conferences;
import com.demonpilot.medicalconferences.R;

import java.util.List;

/**
 * Created by Demon Pilot on 8/23/2016.
 */
public class MyAdapter extends ArrayAdapter<Conferences> {
    private List<Conferences> mConferences;
    public MyAdapter(Context context){
        super(context, R.layout.my_item);
    }

    @Override
    public int getCount() {
        return mConferences.size();
    }

    @Override
    public Conferences getItem(int position) {
        return mConferences.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.my_item, parent,false);
            viewHolder = new ViewHolder();
            viewHolder.topic = (TextView)convertView.findViewById(R.id.topic);
            viewHolder.speaker = (TextView)convertView.findViewById(R.id.speaker);
            viewHolder.date = (TextView)convertView.findViewById(R.id.date);
            viewHolder.startTime = (TextView)convertView.findViewById(R.id.start_time);
            viewHolder.endTime = (TextView)convertView.findViewById(R.id.end_time);
            viewHolder.accept = (TextView)convertView.findViewById(R.id.accept);
            convertView.setTag(viewHolder);
        } else{
            viewHolder = (ViewHolder)convertView.getTag();
        }
        Conferences conferences = getItem(position);
        viewHolder.topic.setText(conferences.getTopic());
        viewHolder.speaker.setText(conferences.getSpeaker());
        viewHolder.date.setText(conferences.getDate());
        viewHolder.startTime.setText(conferences.getStartTime());
        viewHolder.endTime.setText(conferences.getEndTime());
        viewHolder.accept.setText(conferences.getAccept());
        return convertView;
    }

    public void setConferences(List<Conferences> conferences){
        mConferences = conferences;
    }

    private static class ViewHolder{
        TextView topic;
        TextView speaker;
        TextView date;
        TextView startTime;
        TextView endTime;
        TextView accept;
    }
}
