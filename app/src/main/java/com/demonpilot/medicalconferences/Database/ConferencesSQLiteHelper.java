package com.demonpilot.medicalconferences.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import com.demonpilot.medicalconferences.Entities.Conferences;
import com.demonpilot.medicalconferences.Entities.User;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Demon Pilot on 8/22/2016.
 */
public class ConferencesSQLiteHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Conferences.db";

    public static abstract class UserEntry implements BaseColumns{
        public static final String TABLE_NAME = "user";
        public static final String COLUMN_NAME_USER_NAME = "username";
        public static final String COLUMN_NAME_PASSWORD = "password";
    }
    public static abstract class ExistingEntry implements BaseColumns{
        public static final String TABLE_NAME = "existing";
        public static final String COLUMN_NAME_TOPIC = "topic";
        public static final String COLUMN_NAME_SPEAKER = "speaker";
        public static final String COLUMN_NAME_DATE = "date";
        public static final String COLUMN_NAME_START_TIME = "startTime";
        public static final String COLUMN_NAME_END_TIME = "endTime";
    }
    public static abstract class SuggestedEntry implements BaseColumns{
        public static final String TABLE_NAME = "suggested";
        public static final String COLUMN_NAME_TOPIC = "topic";
        public static final String COLUMN_NAME_SPEAKER = "speaker";
        //public static final String COLUMN_NAME_DATE = "date";
        //public static final String COLUMN_NAME_START_TIME = "startTime";
        //public static final String COLUMN_NAME_END_TIME = "endTime";
    }
    public static abstract class MyEntry implements BaseColumns{
        public static final String COLUMN_NAME_TOPIC = "topic";
        public static final String COLUMN_NAME_SPEAKER = "speaker";
        public static final String COLUMN_NAME_DATE = "date";
        public static final String COLUMN_NAME_START_TIME = "startTime";
        public static final String COLUMN_NAME_END_TIME = "endTime";
        public static final String COLUMN_NAME_ACCEPT = "accept";
    }
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private static final String SQL_USER =
            "CREATE TABLE " + UserEntry.TABLE_NAME + " (" +
                    UserEntry.COLUMN_NAME_USER_NAME + TEXT_TYPE + COMMA_SEP +
                    UserEntry.COLUMN_NAME_PASSWORD + TEXT_TYPE +
                    " )";
    private static final String SQL_EXISTING =
            "CREATE TABLE " + ExistingEntry.TABLE_NAME + " (" +
                    ExistingEntry.COLUMN_NAME_TOPIC + TEXT_TYPE + COMMA_SEP +
                    ExistingEntry.COLUMN_NAME_SPEAKER + TEXT_TYPE + COMMA_SEP +
                    ExistingEntry.COLUMN_NAME_DATE + TEXT_TYPE + COMMA_SEP +
                    ExistingEntry.COLUMN_NAME_START_TIME + TEXT_TYPE + COMMA_SEP +
                    ExistingEntry.COLUMN_NAME_END_TIME + TEXT_TYPE +
                    " )";
    private static final String SQL_SUGGESTED =
            "CREATE TABLE " + SuggestedEntry.TABLE_NAME + " (" +
                    SuggestedEntry.COLUMN_NAME_TOPIC + TEXT_TYPE + COMMA_SEP +
                    SuggestedEntry.COLUMN_NAME_SPEAKER + TEXT_TYPE +
                    " )";
    /*private static final String SQL_MY =
            "CREATE TABLE " + MyEntry.TABLE_NAME + " (" +
                    MyEntry.COLUMN_NAME_TOPIC + TEXT_TYPE + COMMA_SEP +
                    MyEntry.COLUMN_NAME_SPEAKER + TEXT_TYPE + COMMA_SEP +
                    MyEntry.COLUMN_NAME_DATE + TEXT_TYPE + COMMA_SEP +
                    MyEntry.COLUMN_NAME_START_TIME + TEXT_TYPE + COMMA_SEP +
                    MyEntry.COLUMN_NAME_END_TIME + TEXT_TYPE +
                    " )";*/
    private static final String SQL_DELETE_USER =
            "DROP TABLE IF EXISTS " + UserEntry.TABLE_NAME;
    private static final String SQL_DELETE_EXISTING =
            "DROP TABLE IF EXISTS " + ExistingEntry.TABLE_NAME;
    private static final String SQL_DELETE_SUGGESTED =
            "DROP TABLE IF EXISTS " + SuggestedEntry.TABLE_NAME;
    //private static final String SQL_DELETE_MY =
    //        "DROP TABLE IF EXISTS " + MyEntry.TABLE_NAME;

    public ConferencesSQLiteHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_USER);
        db.execSQL(SQL_EXISTING);
        db.execSQL(SQL_SUGGESTED);
        //db.execSQL(SQL_MY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_USER);
        db.execSQL(SQL_DELETE_EXISTING);
        db.execSQL(SQL_DELETE_SUGGESTED);
        //db.execSQL(SQL_DELETE_MY);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void dropTable(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(SQL_DELETE_USER);
        db.execSQL(SQL_DELETE_EXISTING);
        db.execSQL(SQL_DELETE_SUGGESTED);
        //db.execSQL(SQL_DELETE_MY);
        onCreate(db);
    }

    public void addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(UserEntry.COLUMN_NAME_USER_NAME, user.getUsername());
        values.put(UserEntry.COLUMN_NAME_PASSWORD, user.getPassword());
        db.insert(UserEntry.TABLE_NAME, null, values);
        db.close();
    }
    public User getUser(String username){
        User user = new User();
        String query = "SELECT * FROM " + UserEntry.TABLE_NAME +
                " WHERE " + UserEntry.COLUMN_NAME_USER_NAME + " = \"" + username + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                user = new User(cursor.getString(0), cursor.getString(1));
            } while (cursor.moveToNext());
        }

        return user;
    }
    public boolean userExist(String username){
        String query = "SELECT * FROM " + UserEntry.TABLE_NAME +
                " WHERE " + UserEntry.COLUMN_NAME_USER_NAME + " = \"" + username + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }
    public void addExisting(Conferences conferences){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ExistingEntry.COLUMN_NAME_TOPIC, conferences.getTopic());
        values.put(ExistingEntry.COLUMN_NAME_SPEAKER, conferences.getSpeaker());
        values.put(ExistingEntry.COLUMN_NAME_DATE, conferences.getDate());
        values.put(ExistingEntry.COLUMN_NAME_START_TIME, conferences.getStartTime());
        values.put(ExistingEntry.COLUMN_NAME_END_TIME, conferences.getEndTime());

        db.insert(ExistingEntry.TABLE_NAME, null, values);
        db.close();
    }
    public List<Conferences> getExisting(){
        List<Conferences> conferences = new LinkedList<>();
        String query = "SELECT * FROM " + ExistingEntry.TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                conferences.add(new Conferences(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4)));
            } while (cursor.moveToNext());
        }

        return conferences;
    }
    public void addSuggested(Conferences conferences){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SuggestedEntry.COLUMN_NAME_TOPIC, conferences.getTopic());
        values.put(SuggestedEntry.COLUMN_NAME_SPEAKER, conferences.getSpeaker());

        db.insert(SuggestedEntry.TABLE_NAME, null, values);
        db.close();
    }
    public List<Conferences> getSuggested(){
        List<Conferences> conferences = new LinkedList<>();
        String query = "SELECT * FROM " + SuggestedEntry.TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                conferences.add(new Conferences(cursor.getString(0), cursor.getString(1)));
            } while (cursor.moveToNext());
        }

        return conferences;
    }
    public void createMy(String tableName) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + tableName);
        String query = "CREATE TABLE " + tableName + " (" +
                MyEntry.COLUMN_NAME_TOPIC + TEXT_TYPE + COMMA_SEP +
                MyEntry.COLUMN_NAME_SPEAKER + TEXT_TYPE + COMMA_SEP +
                MyEntry.COLUMN_NAME_DATE + TEXT_TYPE + COMMA_SEP +
                MyEntry.COLUMN_NAME_START_TIME + TEXT_TYPE + COMMA_SEP +
                MyEntry.COLUMN_NAME_END_TIME + TEXT_TYPE + COMMA_SEP +
                MyEntry.COLUMN_NAME_ACCEPT + TEXT_TYPE +
                " )";
        db.execSQL(query);
    }
    public void addMy(String tableName, Conferences conferences){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(MyEntry.COLUMN_NAME_TOPIC, conferences.getTopic());
        values.put(MyEntry.COLUMN_NAME_SPEAKER, conferences.getSpeaker());
        values.put(MyEntry.COLUMN_NAME_DATE, conferences.getDate());
        values.put(MyEntry.COLUMN_NAME_START_TIME, conferences.getStartTime());
        values.put(MyEntry.COLUMN_NAME_END_TIME, conferences.getEndTime());
        values.put(MyEntry.COLUMN_NAME_ACCEPT, conferences.getAccept());

        db.insert(tableName, null, values);
        db.close();
    }
    public List<Conferences> getMy(String tableName){
        List<Conferences> conferences = new LinkedList<>();
        String query = "SELECT * FROM " + tableName;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                conferences.add(new Conferences(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5)));
            } while (cursor.moveToNext());
        }

        return conferences;
    }
}
