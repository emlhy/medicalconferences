package com.demonpilot.medicalconferences.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.demonpilot.medicalconferences.Adapters.FragmentAdapter;
import com.demonpilot.medicalconferences.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Demon Pilot on 8/21/2016.
 */
public class DoctorVPFragment extends Fragment {
    private ViewPager viewPager;
    private List<Fragment> fragmentList = new ArrayList<Fragment>();
    private List<String> titleList = new ArrayList<String>();
    private FragmentAdapter fragmentAdapter;
    private DoctorMyFragment doctorMyFragment;
    private DoctorSuggestedFragment doctorSuggestedFragment;
    public static String username;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_doctor_vp, container, false);
        username = getArguments().getString("username");
        viewPager = (ViewPager) rootView.findViewById(R.id.pager);

        doctorMyFragment = new DoctorMyFragment();
        doctorSuggestedFragment = new DoctorSuggestedFragment();
        fragmentList.add(doctorMyFragment);
        fragmentList.add(doctorSuggestedFragment);
        titleList.add("My");
        titleList.add("Suggested");

        fragmentAdapter = new FragmentAdapter(getFragmentManager(), fragmentList, titleList);
        viewPager.setAdapter(fragmentAdapter);
        viewPager.setCurrentItem(0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        return rootView;
    }
}
