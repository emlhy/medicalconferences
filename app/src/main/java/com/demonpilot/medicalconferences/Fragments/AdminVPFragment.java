package com.demonpilot.medicalconferences.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.demonpilot.medicalconferences.Adapters.FragmentAdapter;
import com.demonpilot.medicalconferences.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Demon Pilot on 8/21/2016.
 */
public class AdminVPFragment extends Fragment {
    private ViewPager viewPager;
    private List<Fragment> fragmentList = new ArrayList<Fragment>();
    private List<String> titleList = new ArrayList<String>();
    private FragmentAdapter fragmentAdapter;
    private AdminExistingFragment adminExistingFragment;
    private AdminSuggestedFragment adminSuggestedFragment;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_admin_vp, container, false);

        viewPager = (ViewPager) rootView.findViewById(R.id.pager);

        adminExistingFragment = new AdminExistingFragment();
        adminSuggestedFragment = new AdminSuggestedFragment();
        fragmentList.add(adminExistingFragment);
        fragmentList.add(adminSuggestedFragment);
        titleList.add("Existing");
        titleList.add("Suggested");

        fragmentAdapter = new FragmentAdapter(getFragmentManager(), fragmentList, titleList);
        viewPager.setAdapter(fragmentAdapter);
        viewPager.setCurrentItem(0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 0){
                    AdminExistingFragment.listView.setLongClickable(true);
                    AdminSuggestedFragment.listView.setLongClickable(false);
                }
                if(position ==1){
                    AdminExistingFragment.listView.setLongClickable(false);
                    AdminSuggestedFragment.listView.setLongClickable(true);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        return rootView;
    }
}
