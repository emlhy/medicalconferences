package com.demonpilot.medicalconferences.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.demonpilot.medicalconferences.Activities.MainActivity;
import com.demonpilot.medicalconferences.Database.ConferencesSQLiteHelper;
import com.demonpilot.medicalconferences.Entities.User;
import com.demonpilot.medicalconferences.R;

/**
 * Created by Demon Pilot on 8/21/2016.
 */
public class SignUpFragment extends Fragment {
    private ConferencesSQLiteHelper dbHelper;
    EditText username;
    EditText password;
    EditText confirm;
    Button done;
    Button cancel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sign_up, container, false);
        dbHelper = new ConferencesSQLiteHelper(getContext());
        username = (EditText)rootView.findViewById(R.id.user_name);
        password = (EditText)rootView.findViewById(R.id.password);
        confirm = (EditText)rootView.findViewById(R.id.confirm);
        done = (Button)rootView.findViewById(R.id.done);
        cancel = (Button)rootView.findViewById(R.id.cancel);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = username.getText().toString();
                String pass = password.getText().toString();
                if(name.equals("") || pass.equals("")){
                    Toast.makeText(getActivity(), "User name or password cannot be empty", Toast.LENGTH_LONG).show();
                } else {
                    if (pass.equals(confirm.getText().toString())) {
                        if (dbHelper.userExist(name)) {
                            Toast.makeText(getActivity(), "User name exist, please use another one!", Toast.LENGTH_LONG).show();
                        } else {
                            User user = new User();
                            user.setUsername(name);
                            user.setPassword(pass);
                            dbHelper.addUser(user);
                            dbHelper.createMy(name);
                            Toast.makeText(getActivity(), "Successfully signed up!", Toast.LENGTH_LONG).show();
                            broadcastPopBackStack();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Password does not match the confirm password, please try again!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                broadcastPopBackStack();
            }
        });
        return rootView;
    }

    private void broadcastPopBackStack() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_POP_BACK_STACK);
        getActivity().sendBroadcast(intent);
    }
}
