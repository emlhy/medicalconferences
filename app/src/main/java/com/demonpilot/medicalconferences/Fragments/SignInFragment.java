package com.demonpilot.medicalconferences.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.demonpilot.medicalconferences.Activities.MainActivity;
import com.demonpilot.medicalconferences.Database.ConferencesSQLiteHelper;
import com.demonpilot.medicalconferences.Entities.User;
import com.demonpilot.medicalconferences.R;

/**
 * Created by Demon Pilot on 8/21/2016.
 */
public class SignInFragment extends Fragment{
    private ConferencesSQLiteHelper dbHelper;
    Spinner type;
    EditText username;
    EditText password;
    Button signIn;
    Button signUp;
    int accountType;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sign_in, container, false);
        dbHelper = new ConferencesSQLiteHelper(getContext());
        type = (Spinner)rootView.findViewById(R.id.spinner);
        username = (EditText)rootView.findViewById(R.id.user_name);
        password = (EditText)rootView.findViewById(R.id.password);
        signIn = (Button)rootView.findViewById(R.id.sign_in);
        signUp = (Button)rootView.findViewById(R.id.sign_up);
        accountType = 1;

        type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String[] types = getResources().getStringArray(R.array.types);
                if(types[position].equals("Administrator")){
                    username.setText("");
                    password.setText("");
                    signUp.setVisibility(View.INVISIBLE);
                    accountType = 0;
                } else if(types[position].equals("Doctor")){
                    username.setText("");
                    password.setText("");
                    signUp.setVisibility(View.VISIBLE);
                    accountType = 1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = username.getText().toString();
                String pass = password.getText().toString();
                if(name.equals("") || pass.equals("")){
                    Toast.makeText(getActivity(), "User name or password cannot be empty", Toast.LENGTH_LONG).show();
                } else {
                    if (accountType == 0) {
                        if (name.equals(MainActivity.admin) && pass.equals(MainActivity.pw)) {
                            username.setText("");
                            password.setText("");
                            broadcastAdminSignIn();
                        } else {
                            Toast.makeText(getActivity(), "Wrong user name or password, please try again!", Toast.LENGTH_LONG).show();
                        }
                    } else if (accountType == 1) {
                        if (dbHelper.userExist(name)) {
                            if (dbHelper.getUser(name).getPassword().equals(pass)) {
                                username.setText("");
                                password.setText("");
                                broadcastDoctorSignIn(name);
                            } else {
                                Toast.makeText(getActivity(), "Wrong user name or password, please try again!", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Wrong user name or password, please try again!", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        });
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username.setText("");
                password.setText("");
                broadcastSignUp();
            }
        });
        return rootView;
    }

    private void broadcastSignUp(){
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_SIGN_UP);
        getActivity().sendBroadcast(intent);
    }
    private void broadcastAdminSignIn(){
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_ADMIN_SIGN_IN);
        getActivity().sendBroadcast(intent);
    }
    private void broadcastDoctorSignIn(String username){
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_DOCTOR_SIGN_IN);
        intent.putExtra("username", username);
        getActivity().sendBroadcast(intent);
    }
}
