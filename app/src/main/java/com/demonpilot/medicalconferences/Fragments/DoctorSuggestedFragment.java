package com.demonpilot.medicalconferences.Fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.demonpilot.medicalconferences.Adapters.ExistingAdapter;
import com.demonpilot.medicalconferences.Adapters.SuggestedAdapter;
import com.demonpilot.medicalconferences.Database.ConferencesSQLiteHelper;
import com.demonpilot.medicalconferences.Entities.Conferences;
import com.demonpilot.medicalconferences.R;

import java.util.List;

/**
 * Created by Demon Pilot on 8/21/2016.
 */
public class DoctorSuggestedFragment extends Fragment {
    private SuggestedAdapter mAdapter;
    private ConferencesSQLiteHelper mHelper;
    private List<Conferences> mConference;
    private Button suggestNew;
    static ListView listView;
    private int flag = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_doctor_suggested, container, false);
        listView = (ListView)rootView.findViewById(R.id.list_ds);
        suggestNew = (Button)rootView.findViewById(R.id.suggest_new);

        mHelper = new ConferencesSQLiteHelper(getActivity());
        mConference = mHelper.getSuggested();
        mAdapter = new SuggestedAdapter(getActivity());
        mAdapter.setConferences(mConference);
        listView.setAdapter(mAdapter);

        suggestNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newSuggestDialog();
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    protected void newSuggestDialog(){
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        View suggestDialog = layoutInflater.inflate(R.layout.dialog_suggest, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(suggestDialog);
        final EditText topic = (EditText)suggestDialog.findViewById(R.id.topic);
        final EditText speaker = (EditText)suggestDialog.findViewById(R.id.speaker);
        alertDialogBuilder.setTitle("New Conference");
        alertDialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(topic.getText().toString().equals("") || speaker.getText().toString().equals("")){

                } else {
                    mHelper.addSuggested(new Conferences(topic.getText().toString(), speaker.getText().toString()));
                    refreshListView();
                }
            }
        });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        flag++;
        if (isVisibleToUser) {
            if(flag > 2) {
                listView.setEnabled(true);
                refreshListView();
            }
        }
    }

    public void refreshListView(){
        mConference.clear();
        mConference = mHelper.getSuggested();
        mAdapter.setConferences(mConference);
        mAdapter.notifyDataSetChanged();
    }
}
