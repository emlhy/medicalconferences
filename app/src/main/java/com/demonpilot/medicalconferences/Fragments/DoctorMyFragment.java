package com.demonpilot.medicalconferences.Fragments;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.demonpilot.medicalconferences.Adapters.MyAdapter;
import com.demonpilot.medicalconferences.Database.ConferencesSQLiteHelper;
import com.demonpilot.medicalconferences.Entities.Conferences;
import com.demonpilot.medicalconferences.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Demon Pilot on 8/21/2016.
 */
public class DoctorMyFragment extends Fragment {
    private MyAdapter mAdapter;
    private ConferencesSQLiteHelper mHelper;
    private List<Conferences> mConference;
    private String username;
    private String t;
    private String st;
    private String et;
    private int flag = 0;
    public static ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_doctor_my, container, false);
        listView = (ListView)rootView.findViewById(R.id.list_dm);
        username = DoctorVPFragment.username;

        mHelper = new ConferencesSQLiteHelper(getActivity());
        mConference = mHelper.getMy(username);
        mAdapter = new MyAdapter(getActivity());
        mAdapter.setConferences(mConference);
        listView.setAdapter(mAdapter);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        registerForContextMenu(listView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;

        Conferences conferences = mConference.get(info.position);
        if (conferences instanceof Conferences) {
            String topic = conferences.getTopic();
            String accept = conferences.getAccept();
            String date = conferences.getDate();
            String start = conferences.getStartTime();
            String end = conferences.getEndTime();
            t = topic;
            st = date + start;
            et = date + end;
            menu.setHeaderTitle(topic);
            if(accept.equals("new")) {
                menu.add(0, 1, Menu.NONE, "Accept");
                menu.add(0, 2, Menu.NONE, "Reject");
            }
            /*if(accept.equals("accept")) {
                menu.add(0, 1, Menu.NONE, "Reject");
            } else if(accept.equals("reject")){
                menu.add(0, 1, Menu.NONE, "Accept");
            }*/
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if(getUserVisibleHint()) {
            if(this.isVisible()) {
                if (item.getItemId() == 1) {
                    String query = "UPDATE " + username + " SET accept = 'accept' WHERE topic = '" + t + "'";
                    SQLiteDatabase db = mHelper.getWritableDatabase();
                    db.execSQL(query);
                    addToCalendar(st, et, t);
                /*if (item.getTitle().equals("Accept")) {
                    String query = "UPDATE " + username + " SET accept = 'accept' WHERE topic = '" + t + "'";
                    SQLiteDatabase db = mHelper.getWritableDatabase();
                    db.execSQL(query);
                } else if (item.getTitle().equals("Reject")) {
                    String query = "UPDATE " + username + " SET accept = 'reject' WHERE topic = '" + t + "'";
                    SQLiteDatabase db = mHelper.getWritableDatabase();
                    db.execSQL(query);
                }
                mConference.clear();
                mConference = mHelper.getMy(username);
                mAdapter.setConferences(mConference);
                mAdapter.notifyDataSetChanged();*/
                } else if (item.getItemId() == 2) {
                    String query = "UPDATE " + username + " SET accept = 'reject' WHERE topic = '" + t + "'";
                    SQLiteDatabase db = mHelper.getWritableDatabase();
                    db.execSQL(query);
                }
                refreshListView();
            }
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        flag++;
        if (isVisibleToUser && isResumed()) {
            if(flag > 2) {
                listView.setEnabled(true);
                refreshListView();
            }
        }
    }

    public void addToCalendar(String beginTime, String endTime, String title){
        Date begin = null;
        Date end = null;
        try {
            begin = new SimpleDateFormat("MM/dd/yyyyHH:mm").parse(beginTime);
            end = new SimpleDateFormat("MM/dd/yyyyHH:mm").parse(endTime);
        } catch(java.text.ParseException e){

        }
        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra("beginTime", begin);
        //intent.putExtra("allDay", true);
        intent.putExtra("rrule", "FREQ=DAILY");
        intent.putExtra("endTime", end);
        intent.putExtra("title", title);
        startActivity(intent);
    }

    public void refreshListView(){
        mConference.clear();
        mConference = mHelper.getMy(username);
        mAdapter.setConferences(mConference);
        mAdapter.notifyDataSetChanged();
    }
}
