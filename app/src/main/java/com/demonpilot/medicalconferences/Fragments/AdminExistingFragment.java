package com.demonpilot.medicalconferences.Fragments;

import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.demonpilot.medicalconferences.Adapters.ExistingAdapter;
import com.demonpilot.medicalconferences.Database.ConferencesSQLiteHelper;
import com.demonpilot.medicalconferences.Entities.Conferences;
import com.demonpilot.medicalconferences.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Demon Pilot on 8/21/2016.
 */
public class AdminExistingFragment extends Fragment {
    private ExistingAdapter mAdapter;
    private ConferencesSQLiteHelper mHelper;
    private List<Conferences> mConference;
    private int flag = 0;
    private String topic;
    private String speaker;
    private String date;
    private String start;
    private String end;
    private String name;
    public static ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_admin_existing, container, false);
        listView = (ListView)rootView.findViewById(R.id.list_ae);

        mHelper = new ConferencesSQLiteHelper(getActivity());
        mConference = mHelper.getExisting();
        mAdapter = new ExistingAdapter(getActivity());
        mAdapter.setConferences(mConference);
        listView.setAdapter(mAdapter);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        registerForContextMenu(listView);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        flag++;
        if (isVisibleToUser && isResumed()) {
            if(flag > 2) {
                listView.setEnabled(true);
                refreshListView();
            }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;

        Conferences conferences = mConference.get(info.position);
        if (conferences instanceof Conferences) {
            topic = conferences.getTopic();
            speaker = conferences.getSpeaker();
            date = conferences.getDate();
            start = conferences.getStartTime();
            end = conferences.getEndTime();
            menu.setHeaderTitle(topic);
            menu.add(0, 1, Menu.NONE, "Invite");
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if(getUserVisibleHint()) {
            if(this.isVisible()) {
                if (item.getItemId() == 1) {
                    inviteDialog();
                }
            }
        }
        return super.onContextItemSelected(item);
    }

    private void inviteDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("Invite");
        final String[] users = getUser().toArray(new String[getUser().size()]);
        alertDialogBuilder.setSingleChoiceItems(users, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                name = users[which].toString();
            }
        });
        alertDialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mHelper.addMy(name, new Conferences(topic, speaker, date, start, end, "new"));
            }
        });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private ArrayList<String> getUser(){
        ArrayList<String> users = new ArrayList<>();
        String query = "SELECT * FROM user" ;
        SQLiteDatabase db = mHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                users.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        return users;
    }

    public void refreshListView(){
        mConference.clear();
        mConference = mHelper.getExisting();
        mAdapter.setConferences(mConference);
        mAdapter.notifyDataSetChanged();
    }
}
