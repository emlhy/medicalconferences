package com.demonpilot.medicalconferences.Fragments;

import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.demonpilot.medicalconferences.Adapters.ExistingAdapter;
import com.demonpilot.medicalconferences.Adapters.SuggestedAdapter;
import com.demonpilot.medicalconferences.Database.ConferencesSQLiteHelper;
import com.demonpilot.medicalconferences.Entities.Conferences;
import com.demonpilot.medicalconferences.R;

import java.util.List;

/**
 * Created by Demon Pilot on 8/21/2016.
 */
public class AdminSuggestedFragment extends Fragment {
    private SuggestedAdapter mAdapter;
    private ConferencesSQLiteHelper mHelper;
    private List<Conferences> mConference;
    private String topic;
    private String speaker;
    private int flag = 0;
    public static ListView listView;
    private boolean isViewShown = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_admin_suggested, container, false);
        listView = (ListView)rootView.findViewById(R.id.list_as);

        mHelper = new ConferencesSQLiteHelper(getActivity());
        mConference = mHelper.getSuggested();
        mAdapter = new SuggestedAdapter(getActivity());
        mAdapter.setConferences(mConference);
        listView.setAdapter(mAdapter);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        registerForContextMenu(listView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;

        Conferences conferences = mConference.get(info.position);
        if (conferences instanceof Conferences) {
            topic = conferences.getTopic();
            speaker = conferences.getSpeaker();
            menu.setHeaderTitle(topic);
            menu.add(0, 1, Menu.NONE, "Schedule");
            menu.add(0, 2, Menu.NONE, "Remove");
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if(getUserVisibleHint()) {
            if(this.isVisible()) {
                if (item.getItemId() == 1) {
                    AdminExistingFragment.listView.setLongClickable(false);
                    AdminExistingFragment.listView.setEnabled(false);
                    scheduleDialog();
                } else if (item.getItemId() == 2) {
                    String query = "DELETE FROM suggested WHERE topic = '" + topic + "'";
                    SQLiteDatabase db = mHelper.getWritableDatabase();
                    db.execSQL(query);
                    refreshListView();
                }
            }
        }
        return super.onContextItemSelected(item);
    }

    private void scheduleDialog(){
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        View scheduleDialog = layoutInflater.inflate(R.layout.dialog_schedule, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(scheduleDialog);
        final EditText day = (EditText)scheduleDialog.findViewById(R.id.day);
        final EditText month = (EditText)scheduleDialog.findViewById(R.id.month);
        final EditText year = (EditText)scheduleDialog.findViewById(R.id.year);
        final EditText startHour = (EditText)scheduleDialog.findViewById(R.id.start_hour);
        final EditText startMin = (EditText)scheduleDialog.findViewById(R.id.start_min);
        final EditText endHour = (EditText)scheduleDialog.findViewById(R.id.end_hour);
        final EditText endMin = (EditText)scheduleDialog.findViewById(R.id.end_min);

        alertDialogBuilder.setTitle("Schedule");
        alertDialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String d = day.getText().toString();
                String m = month.getText().toString();
                String y = year.getText().toString();
                String sh = startHour.getText().toString();
                String sm = startMin.getText().toString();
                String eh = endHour.getText().toString();
                String em = endMin.getText().toString();
                if(d.equals("") || m.equals("") || y.equals("") || sh.equals("") || sm.equals("") || eh.equals("") || em.equals("")){

                } else {
                    String date = m + "/" + d + "/" + y;
                    String start = sh + ":" + sm;
                    String end = eh + ":" + em;
                    mHelper.addExisting(new Conferences(topic, speaker, date, start, end));
                    String query = "DELETE FROM suggested WHERE topic = '" + topic + "'";
                    SQLiteDatabase db = mHelper.getWritableDatabase();
                    db.execSQL(query);
                    refreshListView();
                }
            }
        });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        flag++;
        if (isVisibleToUser && isResumed()) {
            if(flag > 2) {
                listView.setEnabled(true);
                refreshListView();
            }
        }
        if(getView() != null){
            isViewShown = true;
        } else{
            isViewShown = false;
        }
    }

    public void refreshListView(){
        mConference.clear();
        mConference = mHelper.getSuggested();
        mAdapter.setConferences(mConference);
        mAdapter.notifyDataSetChanged();
    }
}
