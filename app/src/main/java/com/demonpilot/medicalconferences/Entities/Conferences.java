package com.demonpilot.medicalconferences.Entities;

/**
 * Created by Demon Pilot on 8/23/2016.
 */
public class Conferences {
    private String topic;
    private String speaker;
    private String date;
    private String startTime;
    private String endTime;
    private String accept;
    public Conferences(){
        super();
    }
    public Conferences(String topic, String speaker){
        super();
        this.topic = topic;
        this.speaker = speaker;
    }
    public Conferences(String topic, String speaker, String date, String startTime, String endTime){
        super();
        this.topic = topic;
        this.speaker = speaker;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
    }
    public Conferences(String topic, String speaker, String date, String startTime, String endTime, String accept){
        super();
        this.topic = topic;
        this.speaker = speaker;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.accept = accept;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getSpeaker() {
        return speaker;
    }

    public void setSpeaker(String speaker) {
        this.speaker = speaker;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getAccept() {
        return accept;
    }

    public void setAccept(String accept) {
        this.accept = accept;
    }
}
