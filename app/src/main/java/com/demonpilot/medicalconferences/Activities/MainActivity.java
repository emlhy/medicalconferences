package com.demonpilot.medicalconferences.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.demonpilot.medicalconferences.Database.ConferencesSQLiteHelper;
import com.demonpilot.medicalconferences.Entities.Conferences;
import com.demonpilot.medicalconferences.Entities.User;
import com.demonpilot.medicalconferences.Fragments.AdminExistingFragment;
import com.demonpilot.medicalconferences.Fragments.AdminSuggestedFragment;
import com.demonpilot.medicalconferences.Fragments.AdminVPFragment;
import com.demonpilot.medicalconferences.Fragments.DoctorMyFragment;
import com.demonpilot.medicalconferences.Fragments.DoctorVPFragment;
import com.demonpilot.medicalconferences.Fragments.SignInFragment;
import com.demonpilot.medicalconferences.Fragments.SignUpFragment;
import com.demonpilot.medicalconferences.R;

public class MainActivity extends AppCompatActivity {
    public static final String INTENT_ADMIN_SIGN_IN = "INTENT_ADMIN_SIGN_IN";
    public static final String INTENT_DOCTOR_SIGN_IN = "INTENT_DOCTOR_SIGN_IN";
    public static final String INTENT_SIGN_UP = "INTENT_SIGN_UP";
    public static final String INTENT_POP_BACK_STACK = "INTENT_POP_BACK_STACK";

    private AdminSignInBroadcastReceiver _adminSignInBroadcastReceiver;
    private DoctorSignInBroadcastReceiver _doctorSignInBroadcastReceiver;
    private SignUpBroadcastReceiver _signUpBroadcastReceiver;
    private PopBackStackBroadcastReceiver _popBackStackBroadcastReceiver;

    public static final String admin = "admin";
    public static final String pw = "password";
    private ConferencesSQLiteHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new ConferencesSQLiteHelper(this);
        _adminSignInBroadcastReceiver = new AdminSignInBroadcastReceiver();
        _doctorSignInBroadcastReceiver = new DoctorSignInBroadcastReceiver();
        _signUpBroadcastReceiver = new SignUpBroadcastReceiver();
        _popBackStackBroadcastReceiver = new PopBackStackBroadcastReceiver();

        if(findViewById(R.id.fragment_container) != null){
            if (savedInstanceState == null) {
                SignInFragment signInFragment = new SignInFragment();
                getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, signInFragment).commit();
            }
        }

        dbHelper.dropTable();  //Hard code for test
        /*dbHelper.addExisting(new Conferences("Blood Conference", "Adam", "08/15/2016", "12:00", "12:30"));
        dbHelper.addExisting(new Conferences("Brain Conference", "Mike", "08/16/2016", "13:00", "13:30"));
        dbHelper.addExisting(new Conferences("Bone Conference", "David", "08/17/2016", "14:00", "14:30"));
        dbHelper.addExisting(new Conferences("Heart Conference", "John", "08/18/2016", "15:00", "15:30"));
        dbHelper.addExisting(new Conferences("HIV Conference", "George", "08/19/2016", "16:00", "16:30"));
        dbHelper.addUser(new User("hongyi", "hongyi"));
        dbHelper.createMy("hongyi");
        dbHelper.addMy("hongyi", new Conferences("Cancer Conference", "Sara", "08/20/2016", "12:00", "12:30", "accept"));
        dbHelper.addMy("hongyi", new Conferences("Muscle Conference", "Jim", "08/21/2016", "13:00", "13:30", "new"));
        dbHelper.addSuggested(new Conferences("Eye Conference", "Paul"));
        dbHelper.addSuggested(new Conferences("Surgery Conference", "Steve"));
        dbHelper.addSuggested(new Conferences("Medicine Conference", "Lee"));*/

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(_adminSignInBroadcastReceiver);
        unregisterReceiver(_doctorSignInBroadcastReceiver);
        unregisterReceiver(_signUpBroadcastReceiver);
        unregisterReceiver(_popBackStackBroadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(_adminSignInBroadcastReceiver, new IntentFilter(INTENT_ADMIN_SIGN_IN));
        registerReceiver(_doctorSignInBroadcastReceiver, new IntentFilter(INTENT_DOCTOR_SIGN_IN));
        registerReceiver(_signUpBroadcastReceiver, new IntentFilter(INTENT_SIGN_UP));
        registerReceiver(_popBackStackBroadcastReceiver, new IntentFilter(INTENT_POP_BACK_STACK));
    }

    public class AdminSignInBroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            adminSignIn();
        }
    }
    private void adminSignIn(){
        AdminVPFragment frag = new AdminVPFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, frag, "ADMIN");
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public class DoctorSignInBroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            String username = intent.getStringExtra("username");
            if(username != null){
                doctorSignIn(username);
            }
        }
    }
    private void doctorSignIn(String username){
        DoctorVPFragment frag = new DoctorVPFragment();
        Bundle bundle = new Bundle();
        bundle.putString("username", username);
        frag.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, frag, "DOCTOR");
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public class SignUpBroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            signUp();
        }
    }
    private void signUp(){
        SignUpFragment frag = new SignUpFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, frag, "SIGN_UP");
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public class PopBackStackBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void onBackPressed() {
        AdminVPFragment adminVPFragment = (AdminVPFragment) getSupportFragmentManager().findFragmentByTag("ADMIN");
        DoctorVPFragment doctorVPFragment = (DoctorVPFragment) getSupportFragmentManager().findFragmentByTag("DOCTOR");
        if(adminVPFragment != null && adminVPFragment.isVisible()){
            AdminExistingFragment.listView.setEnabled(false);
            AdminSuggestedFragment.listView.setEnabled(false);
        } else if(doctorVPFragment != null && doctorVPFragment.isVisible()){
            DoctorMyFragment.listView.setEnabled(false);
        }
        super.onBackPressed();
    }
}
